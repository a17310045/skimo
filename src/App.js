import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Row, Col, Container } from 'react-bootstrap';
import Component from './components/main.js';

  const App = () => 
  <div className="App"><Component /></div>;
  
export default App;

