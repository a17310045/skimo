import React,{ useEffect, useState} from 'react';
import './main.css'
import ComponentNav from './nav.jsx';
import {Row, Col, Container, Card } from 'react-bootstrap';
import { MDBCol, MDBContainer, MDBRow, MDBFooter} from "mdbreact";




const Funcion = () =>{
  
}



const Component = props =>{
	//guardamos la api
	const Api = 'https://api.jsonbin.io/b/605295617ea6546cf3e18655';
	const [info, setInfo] = useState([]);
	const callApi = async () => {
    const response = await fetch(Api);
    const result = await response.json();
    setInfo(result);
    
 	}
	useEffect(() => {
		
		callApi();
	},[])
    return(
        <div>
        <div><ComponentNav /></div>
        <div>
        <Container>      
        <Row>
            <Col xs={12} md={8}>
                <div>
                <h3 className="titles">Clientes Frecuentes</h3>
					
                { !info ? 'Cargando...':
                info.map((info,index)=>{
					return( 
					<div className="contenedor">
					<Card style={{ width: '20rem', height: '8rem', flex: 1, flexDirection: 'row'  }}>
					<Card.Img className="image" style={{ width: '6rem', height: '6rem', borderRadius: '4rem', padding: '10px' }} variant="left" src={info.image_url} />
					<Card.Body>
					  <Card.Title>{info.name}</Card.Title>
					  <Card.Text>
						{info.id}
					  </Card.Text>
					</Card.Body>
				  	</Card>
					  </div>)
                })}
				
                
                </div>
            </Col>
            <Col xs={12} md={4}>
                <div>
                <h3 className="titles">Sobre nosotros</h3>
                <p>SKIMO es el negocio de Fito y Tavo
                    Un sueño que inició como un pequeño juego
                    Se conocieron en el patio de recreo
                    Hablando de cómics y juegos de video</p>
                <p>Estaban buscando armarla en grande
                    Pero no tenían espacio ni dinero
                    Y gracias a la
                    ayuda de Don Vito
                    Su vieja pizzería rentarla consiguieron</p>
                <p>Entonces reciclando y usando la cabeza
                    Hicieron realidad lo que empezó como un sueño</p>
                <p>SKIMO, SKIMO es el lugar
                    Donde Fito y Tavo van a armarla
                    SKIMO, SKIMO es el lugar de estar</p>
                </div>
            </Col>
        </Row>
        </Container>
        </div>
        
        <div>
        <MDBFooter  className="footer pt-4 mt-4">
      <MDBContainer sticky className="cont text-center text-md-left">
        <MDBRow>
          <MDBCol md="8">
            <p>
              Skimo 2006
            </p>
          </MDBCol>
          <MDBCol md="4">
            <p>Visitanos en redes sociales @skimo</p>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </MDBFooter>
        
        </div>
        </div>
        
    );
}
export default Component;
