import React from 'react';
import './nav.css'
import { BsTriangleFill } from 'react-icons/bs';
import {Navbar,Nav, } from 'react-bootstrap';

const Styles = {
texto: {
    fontWeight: 700,
    fontSize: 25,
    padding: 20,
    color: 'white',
    letterSpacing: 1
},

}
const ComponentNav = props =>{
    return(
    <Navbar className="nav" >
            <Navbar.Brand><BsTriangleFill size ="2.7em" color="#239E57 " /></Navbar.Brand>
            
            <Nav className="mr-auto">
                <Nav.Link style={Styles.texto} >Skimo</Nav.Link>
                <Nav.Link style={Styles.texto}>Menú</Nav.Link>

            </Nav>
            <Nav inline>
            <Nav.Link style={Styles.texto}>Contacto</Nav.Link>
            </Nav>
    </Navbar>
    
    );
}
export default ComponentNav;